       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSLPWI1.

      ******************************************************************
      * Creates extract of LP records for LP Data Strategy into LP_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W6167   04/10/13  DRH   Original
      * W13771  10/06/17  KLM   PROJ13771 RST data integration
      * 13624   12/12/19  KLM		FEE AND BILLING
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSLP
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-LP-REC.
           COPY ODSLPWIREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE SPACES.
       01  SUB-UDF                 PIC S9(5) BINARY.
       01  hold-plan-num           pic x(06) value spaces.
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-LP-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSLPWI1'
                                     ==:XXYY:== BY ==LP==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  LPV-REC.
           COPY AHLPV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSLPWI1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  LP-REC.
           COPY AHLP.
       01  SD-AREA.
           COPY PRMSD.

       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
           MOVE 'OMNI_WI' TO WS-FILE-SYSTEM.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.
 
           PERFORM 2000-READ-OMNI-DATA.
           
           CLOSE EXTRACT-FILE.

           GOBACK.

       2000-READ-OMNI-DATA.
           SET IO-LP-VIEW-FILE TO TRUE.
           MOVE SPACES         TO ODS-LP-REC.
           MOVE LPV-REC        TO LP-REC
           MOVE '000002'      TO AHLP-PLAN-NUM OF LP-REC.
           CALL 'LNLPIO'   USING SD-AREA
                                 IO-LP-PARM
                                 IO-LP-READ-GTEQ
                                 LP-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-LP-EOF   = 'Y'  
                OR IO-LP-ERROR = 'Y'.

       3000-PROCESS-RECORD.
           IF AHLP-REC-TYPE OF LP-REC = 'LP'
              PERFORM 4000-WRITE-EXTRACT.

           CALL 'LNLPIO' USING SD-AREA
                               IO-LP-PARM
                               IO-LP-READ-GT
                               LP-REC.

       4000-WRITE-EXTRACT.
           MOVE WS-FILE-SYSTEM       TO ODS-LP-REC.
           MOVE CORRESPONDING LP-REC TO ODS-LP-REC.
           
           INSPECT AHLP-DATA OF ODS-LP-REC                               13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT AHLP-DATA OF ODS-LP-REC                               13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT AHLP-DATA OF ODS-LP-REC                               13771
                REPLACING ALL X'0C' BY SPACES.                           13771
           
           WRITE ODS-LP-REC.
