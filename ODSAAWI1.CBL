       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSAAWI1.

      ******************************************************************
      * Creates extract of AA records for RS Data Strategy into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      *        20210706         K. MOSS
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSAA
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-AA-REC.
           COPY ODSAAWIREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE SPACES.
       01  SUB-UDF                 PIC S9(5) BINARY.
      
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-AA-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSAAWI1'
                                     ==:XXYY:== BY ==AA==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  AAV-REC.
           COPY MSTAAAAV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSAAWI1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  AA-REC.
           COPY MSTAAAA.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
           MOVE 'OMNI_WI' TO WS-FILE-SYSTEM.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-AA-REC.        
           SET IO-AA-VIEW-FILE TO TRUE.
           MOVE AAV-REC        TO AA-REC.
           MOVE '000002'       TO AA-PLAN-NUM OF AA-REC.
           CALL 'AAAAIO'   USING SD-AREA
                                 IO-AA-PARM
                                 IO-AA-READ-GTEQ
                                 AA-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-AA-EOF = 'Y'
                OR IO-AA-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'AAAAIO' USING SD-AREA
                               IO-AA-PARM
                               IO-AA-READ-GT
                               AA-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-AA-REC.
           MOVE CORRESPONDING AA-REC TO ODS-AA-REC.

           MOVE AA-UDF-FLAG-TBL OF AA-REC TO 
                AA-UDF-FLAG-TBL OF ODS-AA-REC.
           MOVE AA-UDF-TEXT-TBL OF AA-REC TO 
                AA-UDF-TEXT-TBL OF ODS-AA-REC.
                
           INSPECT AA-DATA OF ODS-AA-REC                               
                REPLACING ALL X'00' BY SPACES.                          
           INSPECT AA-DATA OF ODS-AA-REC                               
                REPLACING ALL X'0A' BY SPACES.                           
           INSPECT AA-DATA OF ODS-AA-REC                               
                REPLACING ALL X'0C' BY SPACES.                                

           WRITE ODS-AA-REC.
