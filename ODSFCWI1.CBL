       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSFCWI1.

      ******************************************************************
      * Creates extract of FC records for RS Data Strategy into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W6167   02/11/13  RPS   Original
      * W13771  10/04/17  KLM   PROJ 13771 RST data integration
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSFC
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-FC-REC.
           COPY ODSFCWIREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM           PIC X(10) VALUE SPACES.
       01  SUB-UDF                  PIC S9(5) BINARY.

      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-FC-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSFCWI1'
                                     ==:XXYY:== BY ==FC==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  FCV-REC.
           COPY MSTRFCV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSFCWI1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  FC-REC.
           COPY MSTRFC.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
           MOVE 'OMNI_WI' TO WS-FILE-SYSTEM.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-FC-REC.
           MOVE FCV-REC        TO FC-REC.
           MOVE '000002'       TO FC-PLAN-NUM OF FC-REC.
           CALL 'FNFCIO'   USING SD-AREA
                                 IO-FC-PARM
                                 IO-FC-READ-GTEQ
                                 FC-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-FC-EOF = 'Y'
                OR IO-FC-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'FNFCIO' USING SD-AREA
                               IO-FC-PARM
                               IO-FC-READ-GT
                               FC-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-FC-REC.
           MOVE CORRESPONDING FC-REC TO ODS-FC-REC.

           MOVE FC-PROCESS-SEQ-LIST(1)  TO FND-LST-F1.
           MOVE FC-PROCESS-SEQ-LIST(2)  TO FND-LST-F2.
           MOVE FC-PROCESS-SEQ-LIST(3)  TO FND-LST-F3.
           MOVE FC-PROCESS-SEQ-LIST(4)  TO FND-LST-F4.
           MOVE FC-PROCESS-SEQ-LIST(5)  TO FND-LST-F5.
           MOVE FC-PROCESS-SEQ-LIST(6)  TO FND-LST-F6.
           MOVE FC-PROCESS-SEQ-LIST(7)  TO FND-LST-F7
           MOVE FC-PROCESS-SEQ-LIST(8)  TO FND-LST-F8.
           MOVE FC-PROCESS-SEQ-LIST(9)  TO FND-LST-F9.
           MOVE FC-PROCESS-SEQ-LIST(10) TO FND-LST-F10.
           MOVE FC-PROCESS-SEQ-LIST(11) TO FND-LST-F11.
           MOVE FC-PROCESS-SEQ-LIST(12) TO FND-LST-F12.
           MOVE FC-PROCESS-SEQ-LIST(13) TO FND-LST-F13.
           MOVE FC-PROCESS-SEQ-LIST(14) TO FND-LST-F14.
           MOVE FC-PROCESS-SEQ-LIST(15) TO FND-LST-F15.
           MOVE FC-PROCESS-SEQ-LIST(16) TO FND-LST-F16.
           MOVE FC-PROCESS-SEQ-LIST(17) TO FND-LST-F17.
           MOVE FC-PROCESS-SEQ-LIST(18) TO FND-LST-F18.
           MOVE FC-PROCESS-SEQ-LIST(19) TO FND-LST-F19.
           MOVE FC-PROCESS-SEQ-LIST(20) TO FND-LST-F20.
           MOVE FC-PROCESS-SEQ-LIST(21) TO FND-LST-F21.
           MOVE FC-PROCESS-SEQ-LIST(22) TO FND-LST-F22.
           MOVE FC-PROCESS-SEQ-LIST(23) TO FND-LST-F23.
           MOVE FC-PROCESS-SEQ-LIST(24) TO FND-LST-F24.
           MOVE FC-PROCESS-SEQ-LIST(25) TO FND-LST-F25.
           MOVE FC-PROCESS-SEQ-LIST(26) TO FND-LST-F26.
           MOVE FC-PROCESS-SEQ-LIST(27) TO FND-LST-F27.
           MOVE FC-PROCESS-SEQ-LIST(28) TO FND-LST-F28.
           MOVE FC-PROCESS-SEQ-LIST(29) TO FND-LST-F29.
           MOVE FC-PROCESS-SEQ-LIST(30) TO FND-LST-F30.
           MOVE FC-PROCESS-SEQ-LIST(31) TO FND-LST-F31.
           MOVE FC-PROCESS-SEQ-LIST(32) TO FND-LST-F32.
           MOVE FC-PROCESS-SEQ-LIST(33) TO FND-LST-F33.
           MOVE FC-PROCESS-SEQ-LIST(34) TO FND-LST-F34.
           MOVE FC-PROCESS-SEQ-LIST(35) TO FND-LST-F35.
           MOVE FC-PROCESS-SEQ-LIST(36) TO FND-LST-F36.
           MOVE FC-PROCESS-SEQ-LIST(37) TO FND-LST-F37.
           MOVE FC-PROCESS-SEQ-LIST(38) TO FND-LST-F38.
           MOVE FC-PROCESS-SEQ-LIST(39) TO FND-LST-F39.
           MOVE FC-PROCESS-SEQ-LIST(40) TO FND-LST-F40.
           MOVE FC-PROCESS-SEQ-LIST(41) TO FND-LST-F41.
           MOVE FC-PROCESS-SEQ-LIST(42) TO FND-LST-F42.
           MOVE FC-PROCESS-SEQ-LIST(43) TO FND-LST-F43.
           MOVE FC-PROCESS-SEQ-LIST(44) TO FND-LST-F44.
           MOVE FC-PROCESS-SEQ-LIST(45) TO FND-LST-F45.
         
           
           INSPECT ODS-FC-REC REPLACING ALL X'00' BY SPACES.
           INSPECT ODS-FC-REC REPLACING ALL X'0A' BY SPACES.
           INSPECT ODS-FC-REC REPLACING ALL X'0C' BY SPACES.

           WRITE ODS-FC-REC.
