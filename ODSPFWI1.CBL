       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSPFWI1.

      ******************************************************************
      * Creates extract of PF records 
      *
      * #         DATE    PROG  DESCRIPTION
      * W6167   02/24/20  KLM   Original      
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSPF
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-PF-REC.
           COPY ODSPFWIREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM           PIC X(10) VALUE SPACES.
       01  SUB-UDF                  PIC S9(9) BINARY VALUE ZEROS.

      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-PF-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSPFWI1'
                                     ==:XXYY:== BY ==PF==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  PFV-REC.
           COPY MSTRPFV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSPFWI1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  PF-REC.
           COPY MSTRPF.
       01  SD-AREA.
           COPY PRMSD.

       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.
       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
           MOVE 'OMNI_WI' TO WS-FILE-SYSTEM.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-PF-REC.
           *> Read across plans - don't stop at end of a plan
           SET IO-PF-VIEW-FILE TO TRUE.
           MOVE PFV-REC        TO PF-REC.
           MOVE '000002'       TO PF-PLAN-NUM OF PF-REC.
           CALL 'PTPFIO'   USING SD-AREA
                                 IO-PF-PARM
                                 IO-PF-READ-GTEQ
                                 PF-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-PF-EOF = 'Y'
                OR IO-PF-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           IF PF-SHRS-PUR OF PF-REC > 0
           OR PF-SHRS-SOLD OF PF-REC > 0
           OR PF-SHRS-RCT OF PF-REC > 0
           OR PF-LAST-POST-DATE OF PF-REC > 0
           OR PF-ALLOC-PCT OF PF-REC > 0          
           OR PF-CNTRB OF PF-REC > 0
           OR PF-EARN-REAL OF PF-REC > 0
           OR PF-EARN-GAINLOSS OF PF-REC > 0
           OR PF-EARN-DIV OF PF-REC > 0
           OR PF-FORF-IN OF PF-REC > 0
           OR PF-XFER-IN OF PF-REC > 0
           OR PF-MISC-IN OF PF-REC > 0
           OR PF-CONV-IN OF PF-REC > 0
           OR PF-OTHER-IN OF PF-REC > 0
           OR PF-LOAN-REPAY-PRINC OF PF-REC > 0
           OR PF-LOAN-REPAY-INT OF PF-REC > 0
           OR PF-WDRL OF PF-REC > 0
           OR PF-TERM OF PF-REC > 0
           OR PF-INST-PMT OF PF-REC > 0
           OR PF-FEES OF PF-REC > 0
           OR PF-FORF-OUT OF PF-REC > 0
           OR PF-XFER-OUT OF PF-REC > 0
           OR PF-MISC-OUT OF PF-REC > 0
           OR PF-CONV-OUT OF PF-REC > 0
           OR PF-OTHER-OUT OF PF-REC > 0
           OR PF-INSUR OF PF-REC > 0
           OR PF-LOAN-ISSUE OF PF-REC > 0
           OR PF-PL-XFER-IN OF PF-REC > 0
           OR PF-PL-XFER-OUT OF PF-REC > 0
              PERFORM 4000-WRITE-EXTRACT
           END-IF.

           CALL 'PTPFIO' USING SD-AREA
                               IO-PF-PARM
                               IO-PF-READ-GT
                               PF-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-PF-REC.
           MOVE CORRESPONDING PF-REC TO ODS-PF-REC.

           MOVE PF-PDF-DOLLAR(1)  TO PF-UDF-600. 
           MOVE PF-PDF-DOLLAR(2)  TO PF-UDF-601. 
           MOVE PF-PDF-DOLLAR(3)  TO PF-UDF-602. 
           MOVE PF-PDF-DOLLAR(4)  TO PF-UDF-603. 
           MOVE PF-PDF-DOLLAR(5)  TO PF-UDF-604.
           MOVE PF-PDF-DOLLAR(6)  TO PF-UDF-605.
           MOVE PF-PDF-DOLLAR(7)  TO PF-UDF-606.
           MOVE PF-PDF-DOLLAR(8)  TO PF-UDF-607.
           MOVE PF-PDF-DOLLAR(9)  TO PF-UDF-608.
           MOVE PF-PDF-DOLLAR(10) TO PF-UDF-609.
           MOVE PF-PDF-DOLLAR(11) TO PF-UDF-610.
           MOVE PF-PDF-DOLLAR(12) TO PF-UDF-611.
           MOVE PF-PDF-DOLLAR(13) TO PF-UDF-612.
           MOVE PF-PDF-DOLLAR(14) TO PF-UDF-613.
           MOVE PF-PDF-DOLLAR(15) TO PF-UDF-614.
           MOVE PF-PDF-DOLLAR(16) TO PF-UDF-615.
           MOVE PF-PDF-DOLLAR(17) TO PF-UDF-616.
           MOVE PF-PDF-DOLLAR(18) TO PF-UDF-617.
           MOVE PF-PDF-DOLLAR(19) TO PF-UDF-618.
           MOVE PF-PDF-DOLLAR(20) TO PF-UDF-619.
           MOVE PF-PDF-DOLLAR(21) TO PF-UDF-620.
           MOVE PF-PDF-DOLLAR(22) TO PF-UDF-621.
           MOVE PF-PDF-DOLLAR(23) TO PF-UDF-622.
           MOVE PF-PDF-DOLLAR(24) TO PF-UDF-623.
           MOVE PF-PDF-DOLLAR(25) TO PF-UDF-624.
           MOVE PF-PDF-DOLLAR(26) TO PF-UDF-625.
           MOVE PF-PDF-DOLLAR(27) TO PF-UDF-626.
           MOVE PF-UDF-AMT(1)     TO PF-UDF-627.
           MOVE PF-UDF-AMT(2)     TO PF-UDF-628.
           MOVE PF-UDF-AMT(3)     TO PF-UDF-629.
           MOVE PF-UDF-AMT(4)     TO PF-UDF-630.
           MOVE PF-UDF-AMT(5)     TO PF-UDF-631.
           MOVE PF-UDF-AMT(6)     TO PF-UDF-632.
           MOVE PF-UDF-AMT(7)     TO PF-UDF-633.
           MOVE PF-UDF-AMT(8)     TO PF-UDF-634.
           MOVE PF-UDF-AMT(9)     TO PF-UDF-635.
           MOVE PF-UDF-AMT(10)    TO PF-UDF-636.
           
           INSPECT PF-DATA OF ODS-PF-REC                                 13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT PF-DATA OF ODS-PF-REC                                 13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT PF-DATA OF ODS-PF-REC                                 13771
                REPLACING ALL X'0C' BY SPACES.                           13771

           WRITE ODS-PF-REC.
